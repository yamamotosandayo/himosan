using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class player : MonoBehaviour
{
    public float x; //速さ
    public GameObject PlPrefab; //生み出されるプレハブ
    Vector3 NowPos;
    Vector3 PrevPos;
    // Start is called before the first frame update
    void Start()
    {
        PrevPos = transform.position;
    }

	// Update is called once per frame
	void Update()
	{

        Clamp();

        Application.targetFrameRate = 60; // 60fpsに設定
        NowPos = transform.position; //今の位置を変数に代入
        if(NowPos!= PrevPos)
        {
            GameObject PL = Instantiate(PlPrefab);
            PL.transform.position = this.transform.position; //自分の位置にPLプレハブを生成させる
        }

        //移動
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(x, 0, 0);//左
        }
        
        if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(-x, 0, 0);//右
        }

        if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 0, -x);//上
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, 0, x);//下
        }
    }

    //移動制御
    private Vector3 player_pos;

    void Clamp()
    {
        player_pos = transform.position;

        player_pos.x = Mathf.Clamp(player_pos.x, -29.0f, 29.0f);
        player_pos.z = Mathf.Clamp(player_pos.z, -27.5f, 67.0f);
        transform.position = new Vector3(player_pos.x, player_pos.y, player_pos.z);
    }


    //タグ付き・当たり判定
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "burokku")
        {
            //Destroy(gameObject, 0.2f);
            SceneManager.LoadScene("GameoverScene");
        }


        if (other.gameObject.tag == "goal")
        {
            //Destroy(gameObject, 0.2f);
            SceneManager.LoadScene("GoalScene");
        }
    }
    
}

