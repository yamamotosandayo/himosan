using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class burokkucon : MonoBehaviour
{
    // Start is called before the first frame update
    void goal(Collision Collision)
    {
        if (Collision.gameObject.tag == "goal")
        {
            Destroy(gameObject, 0.2f);
            SceneManager.LoadScene("GameoverScene");
        }

        
    }
}